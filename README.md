# Upload storage reminder
Modifies the upload storage input field to force the user to select the right file system.

## Installation
 1. Install the module.

## Maintainers
* Bart Vanhoutte (Bart Vanhoutte) - https://www.drupal.org/user/1133754
